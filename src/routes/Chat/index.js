import React, { useState, useEffect, useRef } from "react";
import { connect } from 'react-redux';
import clsx from 'clsx';
import {
    Button,
    Container,
    Typography,
    Card,
    CardContent,
    CardActions,
    List,
    ListItem,
    ListItemText,
    TextField,
    Box,
    IconButton,
    Grid,
} from '@material-ui/core';
import ReactPlayer from 'react-player'
import SendIcon from '@material-ui/icons/Send';
import { makeStyles } from '@material-ui/core/styles';
import socketIOClient from "socket.io-client";

const ENDPOINT = "192.168.0.11:3001";
//const ENDPOINT = 'https://apikuepa.herokuapp.com/'

const socket = socketIOClient(ENDPOINT);

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
        alignItems: 'center'
    },
    margin: {
        margin: theme.spacing(1)
    },
    inline: {
        display: 'inline',
    },
    messages: {
        backgroundColor: theme.palette.primary.light,
        width: '40%',
        marginBottom: theme.spacing(1),
        borderRadius: 20,
    },
    messsagesOwner: {
        backgroundColor: theme.palette.secondary.light,
        left: '60%',
    },
    input: {
        width: '100%'
    },
    boxInput: {
        width: '100%',
        flexDirection: 'row',
        display: 'flex',
        alignItems: 'center'
    },
    title: {
        marginBottom: theme.spacing(2),
        color: theme.palette.primary.main
    },
    cardPlayer: {
        marginBottom: '10%',
        display: 'flex',
        justifyContent: 'center'
    }
}));

const Chat = ({ user }) => {
    const classes = useStyles();
    const [response, setResponse] = useState([]);
    const inputMsg = useRef()
    useEffect(() => {
        socket.on('sendFirst', data => {
            setResponse(data);
        });
        socket.on('allMessage', data => {
            setResponse(data)
        });
    }, [response, user]);
    const sendMessage = () => {
        if (inputMsg.current.value) {
            let msg = {
                user: user.data.email,
                msg: inputMsg.current.value,
            }
            socket.emit('message', msg);
            //response.push(msg)
            inputMsg.current.value = ""
        }
    }
    console.log(user)
    return (
        <div className={classes.root}>
            <main className={classes.content}>
                <div className={classes.appBarSpacer} />
                <Container maxWidth="lg" className={classes.container}>
                    <Typography variant="h3" className={classes.title}>
                        Sala de chat
                    </Typography>
                    <Grid item xs={6} sm={10}>
                        <Card className={classes.cardPlayer}>
                            <ReactPlayer url='https://www.youtube.com/watch?v=ysz5S6PUM-U' controls style={{alignSelf: 'center'}} />
                        </Card>
                    </Grid>
                    <Card>
                        <CardContent>
                            <List>
                                {response.map((x, k) => (
                                    <ListItem key={k} className={clsx(classes.messages, x.user === user.data.email && classes.messsagesOwner)}>
                                        <ListItemText
                                            primary={x.msg}
                                            secondary={x.user}
                                        />
                                    </ListItem>
                                ))}
                                {response.length === 0 && (
                                    <ListItem>
                                        <ListItemText
                                            primary="Aun no hay mensajes en este chat"
                                        />
                                    </ListItem>
                                )}
                            </List>
                        </CardContent>
                        <CardActions>
                            <Box className={classes.boxInput}>
                                <TextField
                                    id="outlined-textarea"
                                    label="Responder..."
                                    placeholder="Escribe..."
                                    multiline
                                    variant="outlined"
                                    inputRef={inputMsg}
                                    className={classes.input}   
                                />
                                <IconButton aria-label="delete" className={classes.margin} onClick={sendMessage}>
                                    <SendIcon fontSize="large" />
                                </IconButton>
                            </Box>
                        </CardActions>
                    </Card>
                </Container>
            </main>
        </div>
    );
}

const mapStateToProps = state => ({
    user: state.user
})

export default connect(mapStateToProps)(Chat)