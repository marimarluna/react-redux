export const ADD_USER = 'ADD_USER'
export const LOGOUT = 'LOGOUT'

export const addUser = data => ({
  type: 'ADD_USER',
  isAuth: true,
  data
})

export const logout = () => ({
  type: 'LOGOUT',
  isAuth: false,
  data: {}
})